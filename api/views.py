from django.shortcuts import render
from django.db import connection
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Doctor, Clinics, Review, City
from .serializers import DoctorSerializer, ClinicsSerializer, ReviewSerializer, CitySerializer, ReviewByDoctorIdSerializer
import datetime,json


class DoctorView(viewsets.ModelViewSet):
    queryset = Doctor.objects.all()
    serializer_class = DoctorSerializer

class ClinicsView(viewsets.ModelViewSet):
    queryset = Clinics.objects.all()
    serializer_class = ClinicsSerializer

class ReviewView(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

class CityView(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer


@api_view(['POST'])
def search_doctor(request):
    if request.method == 'POST':
        print(request.data)
        cityname = request.data['city_slug']
        specialities = request.data['specialities']
        region = request.data['region']
        print(cityname, region, specialities)
        
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT * FROM api_doctor LEFT JOIN api_clinics ON api_doctor.clinics_id = api_clinics.clinics_id WHERE api_clinics.city_slug = '" + cityname + "' " + "AND api_clinics.region_name = '" + region + "' " + "AND api_doctor.specialities LIKE" + " '" + "%" + specialities + "%'"
            )
            rows = cursor.fetchall()
        result = []
        for row in rows:
            item = {}
            item['doctor_id']              = row[0]
            item['clinics_id']             = row[1]
            item['name']                   = row[2]
            item['description']            = row[3]
            item['image']                  = row[4]
            item['experience_year']        = row[6]
            item['price']                  = row[7]
            item['rate_value']             = row[8]
            item['education']              = row[9]
            item['work_experience']        = row[10]
            item['serializes_in_diseases'] = row[11]
            item['qualification']          = row[12]
            item['phone']                  = row[15]
            item['lat']                    = row[18]
            item['lng']                    = row[19]
            print(item)
            result.append(item)
            break
            
        print(json.dumps(result,ensure_ascii=False))
        return Response(json.dumps(result,ensure_ascii=False),status=200)


@api_view(['POST'])
def get_reviews_by_doctor_id(request):
    if request.method == 'POST':
        print(request.data)
        doctorID = request.data['doctor_id']

        review = Review.objects.filter(doctor_id=doctorID)
        print(review)
        review_specializer = ReviewByDoctorIdSerializer(review, many=True)
        return Response(review_specializer.data, status=200)


@api_view(['POST'])
def save_review(request):
    if request.method == 'POST':
        print(request.data)
        doctorId = request.data['doctor_id']
        author = request.data['author']
        rating = request.data['rating']

        review = Review()
        review.doctor_id = doctorId
        review.author = author
        if request.data['text'] is not None:
            review.text = request.data['text']
        review.rating = rating
        review.date = datetime.datetime.now()
        review.save()

        response_data = {'type': 'success'}
        return Response(response_data)