# Generated by Django 2.1.5 on 2019-01-22 05:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_doctor_qualification'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='rate_value',
            field=models.FloatField(default=0),
        ),
    ]
